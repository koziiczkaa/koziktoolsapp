import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCog, faColumns, faListUl, faStickyNote, faUser} from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import './SideNav.scss';

export interface SideNavProps {
	opened: boolean;
}

export function SideNav(props: SideNavProps) {
	return <nav className={'navbar' + (props.opened ? ' navbar--active' : '')}>
		<div className='navbar__logo'>
			<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 224 63'>
				<text
					id='KozikToolsApp'
					transform='translate(0 56)'
					fill='#fff'
					fontSize='29'
					fontFamily='Montserrat-Bold, Montserrat'
					fontWeight='700'
				>
					<tspan x='0' y='0'>KozikToolsApp</tspan>
				</text>
				<rect
					id='Rectangle_11'
					data-name='Rectangle 11'
					width='9.886'
					height='32.952'
					rx='4.943'
					transform='translate(134.154) rotate(59)'
					fill='#d7385e'
				/>
				<rect
					id='Rectangle_12'
					data-name='Rectangle 12'
					width='9.886'
					height='32.952'
					rx='4.943'
					transform='translate(154.762 2.16) rotate(59)'
					fill='#ffac16'
				/>
			</svg>
		</div>
		<ul className='navbar__menu'>
			<li className='navbar__item'>
				<FontAwesomeIcon className='navbar__item__icon' icon={faColumns} color='#fff' size='lg'/>
				<span className='navbar__item__span'>Dashboard</span>
			</li>
			<li className='navbar__item navbar__item--active'>
				<FontAwesomeIcon className='navbar__item__icon navbar__item__icon--active' icon={faListUl} color='#fff' size='lg'/>
				<span className='navbar__item__span'>Tasks</span>
			</li>
			<li className='navbar__item'>
				<FontAwesomeIcon className='navbar__item__icon' icon={faStickyNote} color='#fff' size='lg'/>
				<span className='navbar__item__span'>Notes</span>
			</li>
			<li className='navbar__item'>
				<FontAwesomeIcon className='navbar__item__icon' icon={faUser} color='#fff' size='lg'/>
				<span className='navbar__item__span'>Profile</span>
			</li>
			<li className='navbar__item'>
				<FontAwesomeIcon className='navbar__item__icon' icon={faCog} color='#fff' size='lg'/>
				<span className='navbar__item__span'>Settings</span>
			</li>
		</ul>
	</nav>;
}
