import React from 'react';
import '../components/HamburgerButton.scss';

export interface HamburgerButtonProps {
	onClick: () => void;
	opened: boolean;
}

export function HamburgerButton(props: HamburgerButtonProps): JSX.Element {
	return <div className='header-small'>
		<button className={'hamburger' + (props.opened ? ' hamburger--active' : '')} onClick={props.onClick}>
			<span className='hamburger-box'>
				<span className='hamburger-inner'/>
			</span>
		</button>
	</div>;
}
