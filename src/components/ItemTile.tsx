import React from 'react';
import './ItemTile.scss';

export interface ItemTileProps {
	className?: string;
}

export function ItemTile(props: React.PropsWithChildren<ItemTileProps>) {
	return <div className={props.className}>
		{props.children}
	</div>;
}