import React from 'react';
import './TasksHourComponent.scss';

export interface TasksHourComponentProps {
	hour?: string;
}

export function TasksHourComponent(props: TasksHourComponentProps) {
	return <div className='tasks__content'>
		<div className='tasks__element'>
			<div className='tasks__div__hour'>
				<span className='tasks__hour'>{props.hour}</span>
			</div>
			<div className='tasks__div__line'>
				<span className='tasks__line'/>
			</div>
		</div>
	</div>;
}