import React from 'react';
import './NavBar.scss';
import 'boxicons';
import {SideNav} from './SideNav';
import {HamburgerButton} from './HamburgerButton';

export function NavBar() {
	const [open, setOpen] = React.useState<boolean>(false);

	function handleClick() {
		setOpen(!open);
	}

	return <>
		<HamburgerButton opened={open} onClick={handleClick}/>
		<SideNav opened={open}/>
	</>;
}
