import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import * as React from 'react';
import {ItemTile} from './ItemTile';
import {faPlusCircle, faTasks} from '@fortawesome/free-solid-svg-icons';
import './TasksComponent.scss';
import {TasksHourComponent} from './TasksHourComponent';
import {TaskTileComponent} from './TaskTileComponent';

export function TasksComponent() {
    return <>
        <ItemTile className='itemtile'>
            <div className='tasks'>
                <div className='tasks__nav'>
                    <FontAwesomeIcon
                        icon={faTasks}
                        size='2x'
                        color='#FEAC21'
                    />
                    <h2 className='tasks__title'>Tasks</h2>
                    <FontAwesomeIcon
                        icon={faPlusCircle}
                        className='tasks__icons'
                        size='2x'
                        color='#4E5255'
                    />
                </div>
                <TasksHourComponent hour='09:00'/>
                <TasksHourComponent/>
                <TasksHourComponent hour='10:00'/>
                <TasksHourComponent/>
                <TasksHourComponent hour='11:00'/>
                <div className='tasks__components'>
                    <TaskTileComponent
                        title='Meeting'
                        desc='Discuss tools development asdjahsdhajsdasd'
                        hour='09:00 - 9:30'
                    />
                    <TaskTileComponent
                        title='Meeting'
                        desc='Discuss tools development asdjahsdhajsdasd'
                        hour='09:00 - 9:30'
                    />
                </div>
            </div>
        </ItemTile>
    </>;
}