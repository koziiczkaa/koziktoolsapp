import React from 'react';
import './CalendarComponent.scss';
import {faChevronCircleLeft, faChevronCircleRight} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {ItemTile} from './ItemTile';
import {Day} from '../interfaces';

export function CalendarComponent() {
	const [month, setMonth] = React.useState<number>(new Date().getMonth());
	const [year, setYear] = React.useState<number>(new Date().getFullYear());

	const monthNames: string[] = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Aug',
		'Sep',
		'Oct',
		'Nov',
		'Dec'
	];

	function nextMonth(): void {
		if (month === 11) {
			setYear(year + 1);
			setMonth(0);
		} else {
			setMonth(month + 1);
		}
	}

	function prevMonth(): void {
		if (month === 0) {
			setYear(year - 1);
			setMonth(11);
		} else {
			setMonth(month - 1);
		}
	}

	function getLastDays(): number[] {
		const prevDays: number[] = [];
		const weekDay: number = new Date(year, month, 1).getDay() || 7;

		for (let i = weekDay - 2; i >= 0; i--) {
			const lastMonthDate: Date = new Date(year, month, -i);
			prevDays.push(lastMonthDate.getDate());
		}
		return prevDays;
	}

	function getCurrentDays(): Day[] {
		const currentDays: Day[] = [];
		const currentDay: number = new Date().getDate();
		const daysCount: number = new Date(year, month + 1, 0).getDate();
		const currentMonth: number = new Date().getMonth();
		const currentYear: number = new Date().getFullYear();

		for (let i = 1; i <= daysCount; i++) {
			currentDays.push({day: i, current: currentDay === i && currentMonth === month && currentYear === year});
		}
		return currentDays;
	}

	function getNextDays(): number[] {
		const nextDays: number[] = [];
		const daysCount: number = new Date(year, month + 1, 0).getDate();
		const firstWeekDay: number = new Date(year, month, 1).getDay() || 7;
		const lastWeekDay: number = new Date(year, month, daysCount).getDay() || 7;

		if ((firstWeekDay === 7 && daysCount === 30) || (daysCount === 31 && firstWeekDay >= 6)) {
			for (let i = 1; i <= 6; i++) {
				const nextMonthDate = new Date(year, month + 1, i);

				if (nextMonthDate.getDay() === 1) {
					break;
				}

				nextDays.push(i);
			}
			return nextDays;
		}

		if (lastWeekDay === 7 && daysCount > 28) {
			for (let i = 1; i <= 7; i++) {
				nextDays.push(i);
			}
			return nextDays;
		}

		let flag: boolean = false;

		nextDays.push(1);
		for (let i = 2; i <= 14; i++) {
			const nextMonthDate: Date = new Date(year, month + 1, i);

			if (nextMonthDate.getDay() === 1) {
				if (flag) {
					break;
				}
				flag = true;
			}
			nextDays.push(i);
		}
		return nextDays;
	}

	return <>
		<ItemTile className='itemtile'>
			<div className='calendar__wrapper'>
				<div className='calendar__nav'>
					<h1 className='calendar__nav__month'>{monthNames[month]} {year}</h1>
					<FontAwesomeIcon
						className='calendar__nav__arrow'
						icon={faChevronCircleLeft}
						color='#4E5255'
						size='2x'
						onClick={prevMonth}
					/>
					<FontAwesomeIcon
						className='calendar__nav__arrow'
						icon={faChevronCircleRight}
						color='#4E5255'
						size='2x'
						onClick={nextMonth}
					/>
				</div>
				<div className='calendar__content'>
					{['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'].map((weekday: string, index: number) => <div key={index} className='calendar__weekday'>{weekday}</div>)}
					{getLastDays().map((value: number, index: number) => <div key={index} className='calendar__number calendar__number--newmonth'>{value}</div>)}
					{getCurrentDays().map((day: Day, index: number) => <div key={index} className={'calendar__number' + (day.current ? ' calendar__number--current' : '')}>{day.day}</div>)}
					{getNextDays().map((value: number, index: number) => <div key={index} className='calendar__number calendar__number--newmonth'>{value}</div>)}
				</div>
			</div>
		</ItemTile>
	</>;
}
